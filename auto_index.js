(function () {
  "use strict";

  const categories = {
    audio: [
      "aac", "mid", "midi", "mp3", "oga", "opus",
      "wav", "weba", "3gp", "3g2"
    ],
    binary: ["bin", "exe", "jar", "mpkg"],
    compressed: ["bz", "bz2", "gz", "rar", "tar", "zip", "7z"],
    diskimage: ["iso", "cda"],
    document: [
      "abw", "arc", "azw", "doc", "docx", "epub",
      "odp", "pdf", "ppt", "pptx", "rtf", "swf"
    ],
    font: ["eot", "itf", "ttf"],
    image: [
      "avif", "bmp", "gif", "ico", "jpeg", "jpg",
      "png", "svg", "tif", "tiff", "webp"
    ],
    script: ["csh", "sh", "ksh"],
    text: [
      "css", "csv", "htm", "html", "ics", "js", "mjs", "txt",
      "json", "ods", "odt", "php", "xhtml", "xls", "xlsx", "xml", "md"
    ],
    video: ["avi", "mp4", "mpeg", "ogv", "ts", "webm"]
  };

  // TODO: Use FileReader properly
  function isDir(file) {
    return file.type === "" && !file.name.includes(".");
  }

  function getValidFilename(filename) {
    return filename.replace(/[/\\?%*:|"<>]/g, "");
  }

  function getLocalLink(file) {
    const basePath = `./${getValidFilename(file.name)}`;

    if (isDir(file)) {
      return `${basePath}/`;
    }
    return basePath;
  }

  function getCategory(file) {
    if (isDir(file)) {
      return "dir";
    }

    const fileExt = file.name.split(".").pop();

    // Find associated category for extension, else "text"
    return Object.keys(categories).filter(
      (name) => categories[name].includes(fileExt)
    )[0] || "text";
  }

  function getReadableSize(file) {
    if (isDir(file)) {
      return "-";
    }

    if (file.size === 0) {
      return "0B";
    }

    const units = ["B", "K", "M", "G", "T", "P"];
    // Get all possible file sizes e.g [ (1024,B), (1,K), (0,M), ...]
    const rounded = units.map(
      (unit, i) => [Math.round(file.size / Math.pow(1024, i)), unit]
    );

    // Choose one which is greater than 0, but less than 1024
    const readable = rounded.filter((i) => i[0] > 0 && i[0] < 1024);
    // If there is none, pick the last one (0B case handled in earlier if)
    const bestChoice = readable[0] || rounded.pop();

    return bestChoice.join("");
  }

  function getReadableDate(timestamp) {
    const date = new Date(timestamp);

    const [ymd, time] = [
      [date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()],
      [date.getUTCHours(), date.getUTCMinutes()]
    ].map((arr) => arr.map((i) => i.toString().padStart(2, "0")));

    return `${ymd.join("-")} ${time.join(":")}`;
  }

  function makeRow(category, link, filename, lastModified, size) {
    const validFilename = getValidFilename(filename);

    return `      <!-- ROW START: ${validFilename} -->
      <tr>
        <td><img class="icon" src="/icons/${category}.gif" alt="[   ]"></td>
        <td><a href="${link}">${validFilename}</a></td>
        <td class="right">${lastModified}</td>
        <td class="right">${size}</td>
      </tr>
      <!-- ROW END -->`;
  }

  function handleDrop(files, textarea) {
    // Convert each file, to a HTML row and join
    const rows = [...files].map(
      (f) => makeRow(
        getCategory(f),
        getLocalLink(f),
        f.name,
        getReadableDate(f.lastModified),
        getReadableSize(f)
      )
    ).join("\n");

    const signature = "<!-- ROWS WILL BE INSERTED HERE -->";

    // Place HTML rows before the signature.
    textarea.value = textarea.value.replaceAll(
      signature,
      `${rows}\n${signature}`
    );

    // Trigger iFrame update
    textarea.onchange();
  }

  function bindIFrame(iframe, textarea, callback) {
    textarea.value = (
      // Use Serializer gets doctype string, then append to iFrame
      `${new XMLSerializer().serializeToString(
        iframe.contentDocument.doctype
      )}\n${iframe.contentDocument.documentElement.outerHTML}`
    );

    textarea.onchange = function () {
      iframe.src = "about:blank";
      iframe.onload = function () {
        iframe.contentDocument.open();
        iframe.contentDocument.write(textarea.value);
        iframe.contentDocument.close();
        callback(iframe);
      };
    };
  }

  window.addEventListener("load", function () {
    const dropzone = document.querySelector(".app-dropzone");
    const dropzoneInput = document.querySelector("#app-dropzone-input");
    const dropzoneLabel = document.querySelector(
      "label[for=\"app-dropzone-input\"]"
    );
    const previewFrame = document.querySelector("#app-previewFrame");
    const codearea = document.querySelector(".app-textarea");

    bindIFrame(previewFrame, codearea, function (iframe) {
      // Bind dragover event to iFrame whenever it is "reloaded" by inputs
      iframe.contentWindow.ondragover = function (ev) {
        ev.preventDefault();
        // Fade in if dragged over the iFrame
        dropzone.classList.toggle("app-dropzone-fadeout", false);
      };

      window.ondragleave = function (ev) {
        ev.preventDefault();
        // Fade out if dragged back out the window
        dropzone.classList.toggle("app-dropzone-fadeout", true);
      };
    });

    codearea.ondragover = function () {
      window.location.hash = '';
    }

    dropzoneInput.onchange = function () {
      dropzone.classList.toggle("app-dropzone-fadeout", true);
      handleDrop(dropzoneInput.files, codearea);
    };

    dropzoneLabel.ondragover = function (ev) {
      ev.preventDefault();
      dropzone.classList.toggle("app-dropzone-fadeout", false);
    };

    dropzoneLabel.ondrop = function (ev) {
      ev.preventDefault();
      dropzone.classList.toggle("app-dropzone-fadeout", true);
      handleDrop(ev.dataTransfer.files, codearea);
    };
  });
}());
